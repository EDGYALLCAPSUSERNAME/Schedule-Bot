#!/usr/bin/env python3

import time
import praw
import inspect
import sqlite3
import logging
import OAuth2Util
from datetime import datetime

# configure the logger
logging.basicConfig(filename='schedule_bot_log.log',
                    level=logging.INFO,
                    format='%(asctime)s %(message)s')

# User config
# --------------------------------------------------------------------
# filename for the posting schedule, expected format:
# https://www.google.com/,Title,SubredditName,2016-01-03 23:54:02
FNAME = 'posting_schedule.txt'

# This variable will tell the bot to overwrite your schedule file
# getting rid of any already posted links out of the file. NOTE: The 
# bot keeps a database of already posted links to avoid double posting.
OVERWRITE = False

# Set this to your username
USERNAME = ''

# --------------------------------------------------------------------


# this function is formatting the dictionary of variables
# for logging in the event of an exception
def format_var_str(dic):
    s = ''
    for var, val in dic.items():
        s += ('\t' * 8) + '{}: {}\n'.format(var, val)

    return s


def get_schedule():
    with open(FNAME, 'r') as f:
        # generate a list of lists of scheules
        lines = [line.split(',') for line in f.read().splitlines()]

    return lines


def make_post(r, post):
    print('Making post in /r/{}...'.format(post[2]))
    r.submit(post[2], post[1], url=post[0], captcha=None)


def overwrite_file(posts, posted):
    with open(FNAME, 'w') as f:
        for post in posts:
            if post not in posted:
                f.write(','.join(post) + '\n')


def find_posts(r, c, sql, posts):
    posted = []

    for post in posts:
        # check if this link has been submitted to this sub already
        c.execute('SELECT * FROM posts\
                   WHERE url = ? AND subreddit = ?',
                   [post[0], post[2]])
        if c.fetchone():  # skip the post if we found it
            posted.append(post)
            continue

        # break the timestamp down into useable ints for datetime
        # timestamp format: 2016-01-03 23:54:02
        date = [int(t) for t in post[3].split(' ')[0].split('-')]
        time = [int(t) for t in post[3].split(' ')[1].split(':')]
        ts = datetime(year=date[0], month=date[1], day=date[2],
                      hour=time[0], minute=time[1], second=time[2])

        # If the timestamp on the post is older than or equal to the
        # time now make the post, as it hasn't been posted yet
        # NOTE: if your timestamps are in UTC and not your local timezone
        # change datetime.now() to datetime.utcnow()
        if ts <= datetime.now():
            make_post(r, post)
            posted.append(post)

            # add it to the db
            c.execute('INSERT INTO posts VALUES(?,?)', [post[0], post[2]])
            sql.commit()

    return posted

def main():
    r = praw.Reddit(user_agent='Scheduled_Posts v1.0 /u/{}'.format(USERNAME))
    o = OAuth2Util.OAuth2Util(r, print_log=True)
    o.refresh(force=True)  # force it to refresh the token

    print('Loading database...')
    sql = sqlite3.connect('already_posted_links.db')
    c = sql.cursor()
    c.execute('CREATE TABLE IF NOT EXISTS posts(url TEXT, subreddit TEXT)')
    sql.commit()
    print('Database loaded.')

    # Main loop
    while True:
        try:
            posts = get_schedule()  # get the schedule of posts
            posted = find_posts(r, c, sql, posts)  # make posts

            if OVERWRITE:
                # overwrite the file with only upcoming posts
                overwrite_file(posts, posted)
        except Exception as e:
            print('ERROR: {}'.format(e))

            # log the error event with specific information from the traceback
            traceback_log = '''
            ERROR: {e}
            File "{fname}", line {lineno}, in {fn}
            Time of error: {t}

            Variable dump:

            {g_vars}
            {l_vars}
            '''
            # grabs the traceback info
            frame, fname, lineno, fn = inspect.trace()[-1][:-2]
            # dump the variables and get formated strings
            g_vars = 'Globals:\n' + format_var_str(frame.f_globals)
            l_vars = 'Locals:\n' + format_var_str(frame.f_locals)

            logging.error(traceback_log.format(e=e, lineno=lineno, fn=fn,
                                               fname=fname, t=time.strftime('%c'),
                                               g_vars=g_vars, l_vars=l_vars))

        print('Sleeping...')
        time.sleep(60)  # go to sleep for a little bit


if __name__ == '__main__':
    if not FNAME:
        print('ERROR: File name is not set.\nExiting...')
        exit(1)

    if not USERNAME or USERNAME is '' or type(USERNAME) is not str:
        print('ERROR: Username is empty or incorrect type.\nExiting...')
        exit(1)

    main()